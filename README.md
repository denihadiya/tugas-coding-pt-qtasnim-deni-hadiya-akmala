<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p>


## Installation Guide

### Clone Repository

```sh
git clone https://gitlab.com/denihadiya/tugas-coding-pt-qtasnim-deni-hadiya-akmala.git
cd tugas-coding-pt-qtasnim-deni-hadiya-akmala
```

## Install Dependencies
### Make sure you have Composer installed. Run the following command to install all dependencies:

```sh
composer install
```

## Copy .env File
### Copy the .env.example file to .env:
```sh 
cp .env.example .env
```
## Generate Application Key
### Run the following command to generate the application key:
```sh
php artisan key:generate
```
## Configure .env File
### Update the .env file with your database settings:

```sh
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=penjualan_db
DB_USERNAME=root
DB_PASSWORD=
```

## Migrate Database
### Run the following command to migrate the database:

```sh
php artisan migrate
```


## Run the Application
### Start the Laravel development server:

```sh
php artisan serve
```
## Access the Application
### Open your browser and navigate to:
```sh
http://127.0.0.1:8000
```


## MyCV & Portofolio  
### Below is a link to my cv and portfolio website.
```sh
https://brewash.com/

https://sites.google.com/view/denihadiya

https://deni-hadiya-akmala.github.io/
```