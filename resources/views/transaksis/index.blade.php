@extends('layouts.app')

@section('content')
    <style>
        #transaksiTable_filter {
            display: none;
        }
    </style>
    <div class="container mt-4">
        <div class="row">
            <div class="col-12 col-sm-4">
                <h1>Transaksi</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-sm-4">
                <button class="btn btn-primary mb-2" onclick="showCreateForm()">Tambah Transaksi</button><br />
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 col-sm-4">
                Pencarian:
            </div>
            <div class="col-12 col-sm-4">
                <input type="text" id="searchInput" class="form-control" placeholder="Cari Transaksi...">
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 col-sm-4">
                Pengurutan berdasarkan:
            </div>
            <div class="col-12 col-sm-4">
                <select id="sortSelect" class="form-control">
                    <option value="" selected>--Pilih Urutkan--</option>
                    <option value="barang">Nama Barang</option>
                    <option value="tanggal_transaksi">Tanggal Transaksi</option>
                    <option value="stok_terjual">Jumlah Terjual</option>
                </select>
            </div>
            <div class="col-12 col-sm-4">
                <select id="sortOrderSelect" class="form-control">
                    <option value="asc" selected>ASC (Ascending)</option>
                    <option value="desc">DESC (Descending)</option>
                </select>
            </div>
        </div>
        <div class="row my-5">
            <div class="col-12 col-sm-4">
                Filter Tanggal:
            </div>
            <div class="col-12 col-sm-4">
                <input type="text" id="daterange" class="form-control" name="daterange" value="" />
            </div>
        </div>

        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="all-tab" data-toggle="tab" href="#all" role="tab"
                    aria-controls="all" aria-selected="true">Semua Transaksi</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="top-tab" data-toggle="tab" href="#top" role="tab" aria-controls="top"
                    aria-selected="false">Transaksi Terbanyak</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="bottom-tab" data-toggle="tab" href="#bottom" role="tab" aria-controls="bottom"
                    aria-selected="false">Transaksi Terendah</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="all" role="tabpanel" aria-labelledby="all-tab">
                <table class="table table-bordered responsive" id="transaksiTable">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Barang</th>
                            <th>Stok</th>
                            <th>Jumlah Terjual</th>
                            <th>Tanggal Transaksi</th>
                            <th>Jenis Barang</th>
                            <th>Transaksi Oleh</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane fade" id="top" role="tabpanel" aria-labelledby="top-tab">
                <table class="table table-bordered responsive" id="topSellingTable">
                    <thead>
                        <tr>
                            <th>Barang</th>
                            <th>Jumlah Terjual</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="bottom" role="tabpanel" aria-labelledby="bottom-tab">
                <table class="table table-bordered responsive" id="bottomSellingTable">
                    <thead>
                        <tr>
                            <th>Barang</th>
                            <th>Jumlah Terjual</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="transaksiModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formTitle">Create Transaksi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="transaksiForm">
                        @csrf
                        <input type="hidden" id="transaksiId">
                        <div class="form-group">
                            <label>Barang</label>
                            <select id="barang_id" class="form-control" required>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Jumlah Terjual</label>
                            <input type="number" id="stok_terjual" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Tanggal Transaksi</label>
                            <input type="date" id="tanggal_transaksi" class="form-control" required>
                        </div>
                        <div class="form-group d-none">
                            <label>Transaksi Oleh</label>
                            <input type="text" id="user_transactions" class="form-control" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" form="transaksiForm" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            let initialDateRange = $('#daterange').val();

            fetchTopSelling(initialDateRange);
            fetchBottomSelling(initialDateRange);

            var table = $('#transaksiTable').DataTable({
                processing: true,
                serverSide: true,
                autoWidth: true,
                dom: 'Bfrtip',
                ajax: {
                    url: "{{ route('transaksis.index') }}",
                    type: 'GET',
                    data: function(d) {
                        let dateRange = $('#daterange').val();
                        d.dateBetween_p = dateRange;
                    }
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'barang',
                        name: 'barang'
                    },
                    {
                        data: 'stokbarang',
                        name: 'stokbarang'
                    },
                    {
                        data: 'stok_terjual',
                        name: 'stok_terjual'
                    },
                    {
                        data: 'tanggal_transaksi',
                        name: 'tanggal_transaksi'
                    },
                    {
                        data: 'jbarang',
                        name: 'jbarang'
                    },
                    {
                        data: 'user_transactions',
                        name: 'user_transactions'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    }
                ],
                columnDefs: [{
                    targets: [6],
                    orderable: false,
                    visible: false
                }]
            });

            function fetchTopSelling(dateRange) {
                $.ajax({
                    url: "/transaksis/topSelling",
                    method: 'GET',
                    data: {
                        dateBetween_p: dateRange
                    },
                    success: function(response) {
                        var topSellingTable = $('#topSellingTable tbody');
                        topSellingTable.empty();

                        if (response && response.length > 0) {
                            response.forEach(function(item) {
                                topSellingTable.append(
                                    `<tr>
                            <td>${item.barang.nama_barang}</td>
                            <td>${item.total_terjual}</td>
                        </tr>`
                                );
                            });
                        } else {
                            topSellingTable.append('<tr><td colspan="2">No data available</td></tr>');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error('Failed to fetch top selling:', error);
                    }
                });
            }

            function fetchBottomSelling(dateRange) {
                $.ajax({
                    url: "/transaksis/bottomSelling",
                    method: 'GET',
                    data: {
                        dateBetween_p: dateRange
                    },
                    success: function(response) {
                        var bottomSellingTable = $('#bottomSellingTable tbody');
                        bottomSellingTable.empty();

                        if (response && response.length > 0) {
                            response.forEach(function(item) {
                                bottomSellingTable.append(
                                    `<tr>
                            <td>${item.barang.nama_barang}</td>
                            <td>${item.total_terjual}</td>
                        </tr>`
                                );
                            });
                        } else {
                            bottomSellingTable.append(
                                '<tr><td colspan="2">No data available</td></tr>');
                        }
                    },
                    error: function(xhr, status, error) {
                        console.error('Failed to fetch bottom selling:', error);
                    }
                });
            }




            $('#daterange').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY',
                    applyLabel: 'Pilih',
                    cancelLabel: 'Batal',
                    customRangeLabel: 'Rentang Waktu',
                },
                startDate: moment().startOf('month'),
                endDate: moment().endOf('month'),
                ranges: {
                    'Hari Ini': [moment(), moment()],
                    'Minggu Ini': [moment().startOf('week'), moment().endOf('week')],
                    'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
                    'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
                },
            });

            $('#daterange').on('apply.daterangepicker', function(ev, picker) {
                let dateRange = $(this).val();
                table.draw();
                fetchTopSelling(dateRange);
                fetchBottomSelling(dateRange);
            });

            $('#searchInput').on('keyup', function() {
                table.search($(this).val()).draw();
            });

            $('#sortSelect, #sortOrderSelect').on('change', function() {
                let sortBy = $('#sortSelect').val();
                let sortOrderSelect = $('#sortOrderSelect').val();
                if (sortBy) {
                    table.order([table.column(sortBy + ':name').index(), sortOrderSelect]).draw();
                } else {
                    table.order([0, 'asc']).draw();
                }
            });

            fetchBarangsForSelect();

            $('#transaksiForm').on('submit', function(e) {
                e.preventDefault();
                let id = $('#transaksiId').val();
                let url = id ? `/api/transaksis/${id}` : '/api/transaksis';
                let method = id ? 'PUT' : 'POST';

                $.ajax({
                    url: url,
                    method: method,
                    data: {
                        barang_id: $('#barang_id').val(),
                        stok_terjual: $('#stok_terjual').val(),
                        tanggal_transaksi: $('#tanggal_transaksi').val(),
                        user_transactions: $('#user_transactions').val(),
                        _token: '{{ csrf_token() }}'
                    },
                    success: function(response) {
                        $('#transaksiModal').modal('hide');
                        $('#transaksiForm')[0].reset();
                        table.ajax.reload();
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Operation successful!',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Operation failed!',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                $.ajax({
                    url: `/api/transaksis/${id}`,
                    method: 'GET',
                    success: function(response) {
                        $('#formTitle').text('Ubah Transaksi');
                        $('#transaksiId').val(response.id);
                        $('#barang_id').val(response.barang_id);
                        $('#stok_terjual').val(response.stok_terjual);
                        $('#tanggal_transaksi').val(response.tanggal_transaksi);
                        $('#user_transactions').val(response.user_transactions);
                        $('#transaksiModal').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');

                Swal.fire({
                    title: 'Hapus Transaksi',
                    text: 'Apakah ingin menghapus transaksi untuk barang ' + id + '?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Iya'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: `/api/transaksis/${id}`,
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': '{{ csrf_token() }}'
                            },
                            success: function() {
                                table.ajax.reload();
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Deleted',
                                    text: 'Delete operation successful!',
                                    timer: 3000,
                                    showConfirmButton: false
                                });
                            },
                            error: function(xhr, status, error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: 'Delete operation failed!',
                                    timer: 3000,
                                    showConfirmButton: false
                                });
                            }
                        });
                    }
                });
            });
        });

        function fetchBarangsForSelect() {
            $.ajax({
                url: '/api/barangs',
                method: 'GET',
                success: function(response) {
                    console.log(response);
                    if (response.data && Array.isArray(response.data)) {
                        $('#barang_id').empty();
                        response.data.forEach(function(barang) {
                            $('#barang_id').append(
                                `<option value="${barang.id}">${barang.nama_barang}</option>`);
                        });
                    } else {
                        console.error('Invalid response format');
                    }
                },
                error: function(xhr, status, error) {
                    console.error('Failed to fetch barangs:', error);
                }
            });
        }

        function showCreateForm() {
            $('#formTitle').text('Tambah Transaksi');
            $('#transaksiForm')[0].reset();
            $('#transaksiId').val('');
            fetchBarangsForSelect();
            $('#transaksiModal').modal('show');
        }
    </script>
@endsection
