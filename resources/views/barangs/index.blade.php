@extends('layouts.app')

@section('content')
    <div class="container mt-4">
        <h1>Barang</h1>
        <button class="btn btn-primary mb-2 my-5" onclick="showCreateForm()">Tambah Barang</button>
        <table class="table table-bordered" id="barangTable">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Nama Barang</th>
                    <th>Stok</th>
                    <th>Jenis Barang</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- Modal for Create and Edit Barang -->
    <div class="modal fade" id="barangModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="formTitle">Tambah Barang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="barangForm">
                        @csrf
                        <input type="hidden" id="barangId">
                        <div class="form-group">
                            <label>Nama Barang</label>
                            <input type="text" id="nama_barang" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Stok</label>
                            <input type="number" id="stok" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label>Jenis Barang</label>
                            <input type="text" id="jenis_barang" class="form-control" required>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" form="barangForm" class="btn btn-primary">Submit</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            var table = $('#barangTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('barangs.index') }}",
                    type: 'GET'
                },
                columns: [{
                        data: 'id',
                        name: 'id'
                    },
                    {
                        data: 'nama_barang',
                        name: 'nama_barang'
                    },
                    {
                        data: 'stok',
                        name: 'stok'
                    },
                    {
                        data: 'jenis_barang',
                        name: 'jenis_barang'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ]
            });

            $('#barangForm').on('submit', function(e) {
                e.preventDefault();
                let id = $('#barangId').val();
                let url = id ? `/api/barangs/${id}` : '/api/barangs';
                let method = id ? 'PUT' : 'POST';

                $.ajax({
                    url: url,
                    method: method,
                    data: {
                        nama_barang: $('#nama_barang').val(),
                        stok: $('#stok').val(),
                        jenis_barang: $('#jenis_barang').val(),
                        _token: $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function(response) {
                        $('#barangModal').modal('hide');
                        $('#barangForm')[0].reset();
                        table.ajax.reload();
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'Operation successful!',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    },
                    error: function(xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: 'Operation failed!',
                            timer: 3000,
                            showConfirmButton: false
                        });
                    }
                });
            });

            $(document).on('click', '.edit', function() {
                let id = $(this).data('id');
                $.ajax({
                    url: `/api/barangs/${id}`,
                    method: 'GET',
                    success: function(response) {
                        $('#formTitle').text('Ubah Barang');
                        $('#barangId').val(response.id);
                        $('#nama_barang').val(response.nama_barang);
                        $('#stok').val(response.stok);
                        $('#jenis_barang').val(response.jenis_barang);
                        $('#barangModal').modal('show');
                    }
                });
            });

            $(document).on('click', '.delete', function() {
                let id = $(this).data('id');
                let nama_brg = $(this).data('nama_brg');
                
                Swal.fire({
                    title:  'Hapus Barang',
                    text: 'Apakah ingin menghapus nama barang '+ nama_brg +'?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.isConfirmed) {
                        $.ajax({
                            url: `/api/barangs/${id}`,
                            method: 'DELETE',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            success: function() {
                                table.ajax.reload();
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Deleted',
                                    text: 'Delete operation successful!',
                                    timer: 3000,
                                    showConfirmButton: false
                                });
                            },
                            error: function(xhr, status, error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Error',
                                    text: 'Delete operation failed!',
                                    timer: 3000,
                                    showConfirmButton: false
                                });
                            }
                        });
                    }
                });
            });

        });

        function showCreateForm() {
            $('#formTitle').text('Tambah Barang');
            $('#barangForm')[0].reset();
            $('#barangId').val('');
            $('#barangModal').modal('show');
        }
    </script>
@endsection
