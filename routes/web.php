<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return Redirect::to('/api/barangs');
});
Route::get('/transaksis/topSelling', [TransaksiController::class, 'getTopSelling'])->name('transaksis.topSelling');
Route::get('/transaksis/bottomSelling', [TransaksiController::class, 'getBottomSelling'])->name('transaksis.bottomSelling');
