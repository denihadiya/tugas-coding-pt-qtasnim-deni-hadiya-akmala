<?php

namespace App\Http\Controllers;

use App\Models\Transaksi;
use App\Models\Barang;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            // Mulai query builder
            $query = Transaksi::with('barang');

            // Filter berdasarkan rentang tanggal jika tersedia
            $dateBetween = $this->getDateBetween($request);

            // Menggunakan when untuk filter tanggal
            $query->when(count($dateBetween) === 2, function ($q) use ($dateBetween) {
                return $q->whereDate('tanggal_transaksi', '>=', $dateBetween[0])
                    ->whereDate('tanggal_transaksi', '<=', $dateBetween[1]);
            });

            return DataTables::of($query)
                ->addColumn('barang', function ($row) {
                    return $row->barang->nama_barang;
                })
                ->addColumn('jbarang', function ($row) {
                    return $row->barang->jenis_barang;
                })
                ->addColumn('stokbarang', function ($row) {
                    return $row->barang->stok;
                })
                ->addColumn('action', function ($row) {
                    $btn = '<button class="btn btn-warning btn-sm edit" data-id="' . $row->id . '">Ubah</button>';
                    $btn .= ' <button class="btn btn-danger btn-sm delete" data-id="' . $row->id . '">Hapus</button>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('transaksis.index');
    }

    public function getTopSelling(Request $request)
    {
        $dateBetween = $this->getDateBetween($request);

        $topSelling = Transaksi::with('barang')
            ->select('barang_id', DB::raw('SUM(stok_terjual) as total_terjual'))
            ->when(count($dateBetween) === 2, function ($q) use ($dateBetween) {
                return $q->whereDate('tanggal_transaksi', '>=', $dateBetween[0])
                    ->whereDate('tanggal_transaksi', '<=', $dateBetween[1]);
            })
            ->groupBy('barang_id')
            ->orderBy('total_terjual', 'desc')
            ->take(5)
            ->get();

        return response()->json($topSelling);
    }

    public function getBottomSelling(Request $request)
    {
        $dateBetween = $this->getDateBetween($request);

        $bottomSelling = Transaksi::with('barang')
            ->select('barang_id', DB::raw('SUM(stok_terjual) as total_terjual'))
            ->when(count($dateBetween) === 2, function ($q) use ($dateBetween) {
                return $q->whereDate('tanggal_transaksi', '>=', $dateBetween[0])
                    ->whereDate('tanggal_transaksi', '<=', $dateBetween[1]);
            })
            ->groupBy('barang_id')
            ->orderBy('total_terjual', 'asc')
            ->take(5)
            ->get();

        return response()->json($bottomSelling);
    }

    private function getDateBetween(Request $request)
    {
        $dateBetween = [];

        if ($request->input('dateBetween_p')) {
            $filter_tanggal = explode(" - ", $request->input('dateBetween_p'));

            if (count($filter_tanggal) === 2) {
                foreach ($filter_tanggal as $date) {
                    $dateObject = Carbon::createFromFormat('d/m/Y', trim($date));
                    if ($dateObject !== false) {
                        $dateBetween[] = $dateObject->format('Y-m-d');
                    }
                }
            }
        }

        return $dateBetween;
    }
    /////////////////////////////




    public function store(Request $request)
    {
        $validated = $request->validate([
            'barang_id' => 'required|exists:barangs,id',
            'stok_terjual' => 'required|integer',
            'tanggal_transaksi' => 'required|date',
            // 'user_transactions' => 'required|string|max:255', //penambahan field
        ]);

        $transaksi = Transaksi::create($validated);

        return response()->json($transaksi, 201);
    }

    public function show($id)
    {
        return response()->json(Transaksi::with('barang')->findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $transaksi = Transaksi::findOrFail($id);

        $validated = $request->validate([
            'barang_id' => 'sometimes|required|exists:barangs,id',
            'stok_terjual' => 'sometimes|required|integer',
            'tanggal_transaksi' => 'sometimes|required|date',
            // 'user_transactions' => 'sometimes|required|string|max:255', //penambahan field
        ]);

        $transaksi->update($validated);

        return response()->json($transaksi);
    }

    public function destroy($id)
    {
        Transaksi::destroy($id);

        return response()->json(null, 204);
    }
}
