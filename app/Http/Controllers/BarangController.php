<?php

namespace App\Http\Controllers;
use App\Models\Barang;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BarangController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Barang::all();
            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $btn = '<button class="btn btn-warning btn-sm edit" data-id="'.$row->id.'">Ubah</button>';
                    $btn .= ' <button class="btn btn-danger btn-sm delete" data-id="'.$row->id.'" data-nama_brg="'.$row->nama_barang.'">Hapus</button>';
                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('barangs.index');
    }

    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama_barang' => 'required|string|max:255',
            'stok' => 'required|integer',
            'jenis_barang' => 'required|string|max:255',
        ]);

        $barang = Barang::create($validated);

        return response()->json($barang, 201);
    }

    public function show($id)
    {
        return response()->json(Barang::findOrFail($id));
    }

    public function update(Request $request, $id)
    {
        $barang = Barang::findOrFail($id);

        $validated = $request->validate([
            'nama_barang' => 'sometimes|required|string|max:255',
            'stok' => 'sometimes|required|integer',
            'jenis_barang' => 'sometimes|required|string|max:255',
        ]);

        $barang->update($validated);

        return response()->json($barang);
    }

    public function destroy($id)
    {
        Barang::destroy($id);

        return response()->json(null, 204);
    }
}
